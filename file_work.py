import neural_details.first_layer as fl
import datetime


def save_to_file(layer: fl.FirstLayer):
    date = int(datetime.datetime.now().timestamp())
    with open(f"model_{date}.ncgm", "w") as output:
        output.write(f"{layer.v_cell.scalar} {layer.lr} {layer.mh_scalar} {layer.radius} {layer.an} {layer.theta}\n")
        for plane in layer.planes:
            output.write(f"{plane.main_neuron[0]} {plane.main_neuron[1]}\n")
            for row in plane.kernel.weights:
                output.write(" ".join([str(nmb) for nmb in row]))
                output.write("\n")


def load_from_file(filename: str) -> fl.FirstLayer:
    with open(filename, "r") as inp:
        layer_init = inp.readline()
        p = layer_init.split()
        layer = fl.FirstLayer(fl.VCell(float(p[0])), float(p[1]), float(p[2]), int(p[3]), int(p[4]), float(p[5]))
        while True:
            p = inp.readline()
            if p == "":
                return layer
            else:
                l = 2 * layer.radius + 1
                point = (int(p.split()[0]), int(p.split()[1]))
                weights = []
                for i in range(l):
                    line = inp.readline()
                    floats = [float(nmb) for nmb in line.split()]
                    weights.append(floats)
                layer.planes.append(fl.Plane(fl.create_neurons(layer.theta, l),
                                             fl.Kernel(weights, layer.radius), point, layer.an))
