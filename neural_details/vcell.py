import math

import utils


class VCell:

    def __init__(self, scalar: float, weight: float = 0.0):
        self.scalar = scalar
        self.weight = weight

    def activate(self, prev_layer, neuron, radius: int):
        pos = neuron.position
        ans = 0
        for plane in prev_layer.planes:
            for x in range(-radius, radius + 1):
                for y in range(-radius, radius + 1):
                    c = utils.mexican_hat_wavelet(self.scalar, x, y)
                    ans += c * pow(plane.neurons[pos[0] + x][pos[1] + y], 2)
        return math.sqrt(ans)

    def image_activate(self, image, neuron, radius: int):
        pos = neuron.position
        ans = 0
        l = len(image)
        for x in range(-radius, radius + 1):
            for y in range(-radius, radius + 1):
                c = utils.mexican_hat_wavelet(self.scalar, x, y)
                if pos[0] + x >= l or pos[0] + x < 0 or pos[1] + y >= l or pos[1] + y < 0:
                    ans += 0
                else:
                    ans += c * pow(image[pos[0] + x][pos[1] + y], 2)
        if ans < 0:
            # probably something like 5 e-270
            # if ans < -0.0005:
            #   print("big negative weight in vcell after activation, is everything ok??")
            return 0
        else:
            return math.sqrt(ans)

    def hebb_rule(self, kernels: list):
        ans = 0.0
        for kernel in kernels:
            for i in range(len(kernel.weights)):
                for j in range(len(kernel.weights[i])):
                    ans += pow(kernel.weights[i][j], 2) / utils.mexican_hat_wavelet(self.scalar, i, j)
        if ans < 0:
            # probably something like 5 e-270
            # if ans < -0.0005:
            # print("big negative weight in vcell after hebb, is everything ok??")
            self.weight = 0
        else:
            self.weight = math.sqrt(ans)
