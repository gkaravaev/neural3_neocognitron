import utils
from neural_details.vcell import VCell


# константы - theta
# neuron

class Neuron:
    # ok
    # нейрон можно не обнулять, т.к. аутпут не += а =
    def __init__(self, theta: float, position: tuple, output=0.0):
        self.id = utils.id_generator()
        self.output = output
        self.theta = theta
        self.position = position

    def activate(self, prev_layer, v_cell, kernel):
        # using formula for simple cell
        coeff = self.theta / (1 - self.theta)
        v_link = 0
        for row in kernel.weights:
            for weight in row:
                v_link += pow(weight, 2)
        ans = 1
        for plane in prev_layer.planes:
            ans += kernel.convolution(plane.neurons, self.position)
        # do not forget about v-cell
        ans = ans / (1 + self.theta * v_cell.weight * v_cell.activate(prev_layer, self, kernel.radius))
        self.output = utils.heaviside(ans - 1) * coeff


class Kernel:
    def __init__(self, weights: list, radius: int):
        self.weights = weights
        self.radius = radius

    def hebb_rule(self, prev_layer_neurons: list, center: tuple, lr: float, scalar: float):
        for i in range(len(self.weights)):
            for j in range(len(self.weights)):
                x = center[0] + i
                y = center[1] + j
                self.weights[i][j] += lr * utils.mexican_hat_wavelet(scalar, i, j) * prev_layer_neurons[x][y]

    def convolution(self, prev_layer_neurons: list, center: tuple) -> float:
        ans = 0.0
        for i in range(-self.radius, self.radius + 1):
            for j in range(-self.radius, self.radius + 1):
                # x in [center - r; center + r], y in [center - r; center + r]
                x = i + center[0]
                y = j + center[1]
                if (x < 0) or (y < 0) or (x >= len(prev_layer_neurons)) or (y >= len(prev_layer_neurons[0])):
                    # if window has fallen out of matrix
                    ans += 0
                else:
                    # if window did not fall out of matrix
                    weight = self.weights[self.radius + i][self.radius + j]
                    ans += weight * prev_layer_neurons[x][y].output
        return ans


class Plane:
    # plane consists of 2d array of neurons and kernel linked to this plane
    # main_neuron - position the only neuron working, neighbours - how many neurons around him are working
    def __init__(self, neurons: list, kernel: Kernel, main_neuron: tuple, neighbours: int):
        self.neurons = neurons
        self.kernel = kernel
        self.main_neuron = main_neuron
        self.neighbours = neighbours

    def hebb_rule(self, lr: float, prev_layer, scalar: float):
        for plane in prev_layer.planes:
            self.kernel.hebb_rule(plane.neurons, self.main_neuron, lr, scalar)

    def activate(self, prev_layer, v_cell):
        x = self.main_neuron[0]
        y = self.main_neuron[1]
        for i in range(x - self.neighbours, x + self.neighbours):
            for j in range(y - self.neighbours, y + self.neighbours):
                self.neurons[i][j].activate(prev_layer, v_cell, self.kernel)


class ConvLayer:

    # s-layer is a list of planes + v_cell (or v_cells)

    def __init__(self, planes: list, v_cell: VCell):
        self.planes = planes
        self.v_cell = v_cell

    def activate(self, prev_layer, lr: float):
        # обучаем ядра
        for plane in self.planes:
            plane.hebb_rule(lr, prev_layer, self.v_cell.scalar)
        # ставим вес для v_cell
        self.v_cell.hebb_rule(list(map(lambda x: x.kernel, self.planes)))
        # обучаем слой
        for plane in self.planes:
            plane.activate(prev_layer, self.v_cell)
