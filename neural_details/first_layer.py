import math

import utils
from neural_details.vcell import VCell
import numpy as np


def create_neurons(theta, length):
    neurons = []
    for i in range(length):
        neurons.append([])
        for j in range(length):
            neurons[i].append(Neuron(theta, (j, i)))
    return neurons


class Neuron:
    # ok
    # нейрон можно не обнулять, т.к. аутпут не += а =
    def __init__(self, theta: float, position: tuple, output=0.0):
        self.id = utils.id_generator()
        self.output = output
        self.theta = theta
        self.position = position

    def activate(self, image, v_cell, kernel):
        # using formula for simple cell
        coeff = self.theta / (1 - self.theta)
        v_link = 0
        for row in kernel.weights:
            for weight in row:
                v_link += pow(weight, 2)
        ans = 1
        ans += kernel.convolution(image, self.position)
        # do not forget about v-cell
        ans = ans / (1 + self.theta * v_cell.weight * v_cell.image_activate(image, self, kernel.radius))
        self.output = utils.heaviside(ans - 1) * coeff


class Kernel:
    def __init__(self, weights: list, radius: int):
        self.weights = weights
        self.radius = radius

    def hebb_rule(self, image: list, center: tuple, lr: float, scalar: float):
        for i in range(len(self.weights)):
            for j in range(len(self.weights)):
                x = center[0] + i
                y = center[1] + j
                self.weights[i][j] += lr * utils.mexican_hat_wavelet(scalar, i, j) * image[x][y]

    def convolution(self, image: list, center: tuple) -> float:
        ans = 0.0
        for i in range(-self.radius, self.radius + 1):
            for j in range(-self.radius, self.radius + 1):
                # x in [center - r; center + r], y in [center - r; center + r]
                x = i + center[0]
                y = j + center[1]
                if (x < 0) or (y < 0) or (x >= len(image)) or (y >= len(image[0])):
                    # if window has fallen out of matrix
                    ans += 0
                else:
                    # if window did not fall out of matrix
                    weight = self.weights[self.radius + j][self.radius + i]
                    ans += weight * image[y][x]
        return ans


class Plane:
    # plane consists of 2d array of neurons and kernel linked to this plane
    # main_neuron - position the only neuron working, neighbours - how many neurons around him are working
    def __init__(self, neurons: list, kernel: Kernel, main_neuron: tuple, neighbours: int):
        self.neurons = neurons
        self.kernel = kernel
        self.main_neuron = main_neuron
        self.neighbours = neighbours

    def activate(self, image, v_cell):
        # тут порядок x и y не важен, главное просто все нейроны активировать в нужном квадрате
        for x in range(self.main_neuron[0] - self.neighbours, self.main_neuron[0] + self.neighbours + 1):
            for y in range(self.main_neuron[1] - self.neighbours, self.main_neuron[1] + self.neighbours + 1):
                self.neurons[x][y].activate(image, v_cell, self.kernel)

    def to_ints(self):
        return list(map(lambda row: list(map(lambda neuron: neuron.output, row)), self.neurons))


class FirstLayer:
    # if radius = 1 it means 3x3 cells, 2 - 5x5 and so on
    # by default one square is 5x5 (5x5 receptive zone + 3x3 neurons)
    def __init__(self, v_cell: VCell, lr: float, mh_scalar: float = 0.75, radius: int = 2, active_neighbs: int = 3,
                 theta: float = 0.5):
        self.v_cell = v_cell
        self.radius = radius
        self.an = active_neighbs
        self.lr = lr
        self.planes = []
        self.offset = self.radius
        self.mh_scalar = mh_scalar
        self.theta = theta

    def hebb_rule(self, position, image):
        weights = []
        for a in range(2 * self.radius + 1):
            weights.append([])
            for b in range(2 * self.radius + 1):
                weights[a].append(0.0)
        for x in range(position[0] - self.radius, position[0] + self.radius + 1):
            for y in range(position[1] - self.radius, position[1] + self.radius + 1):
                i = y + self.radius - position[1]
                j = x + self.radius - position[0]
                new_weight = image[y][x] * self.lr * utils.mexican_hat_wavelet(self.mh_scalar, i - self.radius,
                                                                               j - self.radius)
                weights[i][j] += new_weight
        return weights

    def scan_image(self, image):
        new_planes = []
        square_size = 2 * self.offset + 1
        for i in range(math.floor(len(image) / square_size)):
            for j in range(math.floor(len(image[0]) / square_size)):
                x = i * square_size + self.offset
                y = j * square_size + self.offset
                weights = self.hebb_rule((y, x), image)
                is_old = False
                for kernel in list(map(lambda x1: x1.kernel, self.planes + new_planes)):
                    if kernel.weights == weights:
                        is_old = True
                        break
                if not is_old:
                    plane = Plane(self.create_neurons(len(image)), Kernel(weights, self.radius), (y, x), self.an)
                    new_planes.append(plane)
        return new_planes

    def build(self, image, instant_test=False):
        image = np.divide(image, 255)
        new_planes = self.scan_image(image)
        self.v_cell.hebb_rule(list(map(lambda x1: x1.kernel, self.planes + new_planes)))
        for plane in new_planes:
            plane.activate(image, self.v_cell)
        self.planes += [plane for plane in new_planes if np.count_nonzero(plane.to_ints()) > 1]
        if instant_test:
            for plane in self.planes:
                plane.activate(image, self.v_cell)

    def test_activate(self, image):
        # на самом деле не очень верная функция, т.к. сохраняет признаки только из последней картинки
        # + те старые которые сработали
        # но довольно полезна для наглядности и дебага
        image = np.divide(image, 255)
        new_planes = self.scan_image(image)
        self.planes += new_planes
        self.v_cell.hebb_rule(list(map(lambda x1: x1.kernel, self.planes)))
        for plane in self.planes:
            plane.activate(image, self.v_cell)
        # какой ужас
        self.planes[:] = [plane for plane in self.planes if not np.sum(np.absolute(plane.to_ints())) < 0.00005]
