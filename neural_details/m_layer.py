import utils
from neural_details.s_layer import ConvLayer


class Neuron:
    # ok
    def __init__(self, scalar: float, radius: int, position: tuple):
        self.id = utils.id_generator()
        self.output = 0.0
        self.position = position
        self.scalar = scalar
        self.radius = radius

    def activate(self, prev_layer: ConvLayer, plane_index: int, offset: float):
        ans = 0.0
        plane = prev_layer.planes[plane_index]
        for i in range(-self.radius, self.radius + 1):
            for j in range(-self.radius, self.radius + 1):
                x = offset * self.position[0] + i + self.radius
                y = offset * self.position[1] + j + self.radius
                ans += plane.neurons[x][y].output * utils.mexican_hat_wavelet(self.scalar, i, j)
        self.output = utils.mp_func(ans)


class Plane:
    def __init__(self, neurons: list):
        self.neurons = neurons

    def activate(self, prev_layer: ConvLayer, plane_index: int, offset: float):
        for neuron_row in self.neurons:
            for neuron in neuron_row:
                neuron.activate(prev_layer, plane_index, offset)


class MPLayer:

    def __init__(self, planes: list, offset: int):
        self.planes = planes
        self.offset = offset

    def activate(self, prev_layer: list):
        # prevLayer - list of planes
        for index, plane in enumerate(self.planes):
            plane.activate(prev_layer, index, self.offset)
