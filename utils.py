import math


def id_generator():
    id_generator.counter += 1
    return id_generator.counter


id_generator.counter = 0


def mexican_hat_wavelet(scalar: float, x: int, y: int):
    # -len(kernel)/2 <= x = y <= len(kernel)/2
    exp = math.exp(-1 * (pow(x, 2) + pow(y, 2)) / (2 * pow(scalar, 2)))
    div = math.pi * pow(scalar, 4)
    return (1 - 0.5 * ((pow(x, 2) + pow(y, 2)) / pow(scalar, 2))) * exp / div


def heaviside(x: float):
    return max(0.0, x)


def mp_func(x: float):
    return max(0.0, x) / (1 + max(0.0, x))


def hebb_rule(lr: float, position: tuple, prev_output: float, scalar: float):
    return lr * mexican_hat_wavelet(scalar, position[0], position[1]) * prev_output

