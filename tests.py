import sys

import file_work
from neural_details import s_layer, first_layer
from mnist import train_images, train_labels
import matplotlib.pyplot as plt
import numpy as np
import utils
from neural_details.vcell import VCell


def test1():
    kernel = s_layer.Kernel([[1, 1, 1], [1, 1, 1], [1, 1, 1]])
    neuron_list = []
    for i in range(3):
        neuron_list.append([])
        for j in range(3):
            neuron_list[i].append(s_layer.Neuron(theta=0.0, position=(i, j)))
            neuron_list[i][j].output = 1

    plane = s_layer.Plane(neuron_list, kernel)


def test_mh_scalar():
    wave = []
    for i in range(-2, 3):
        wave.append([])
        for j in range(-2, 3):
            wave[i + 2].append(utils.mexican_hat_wavelet(0.7511, i, j))
        # print(wave[i + 5])
    x = np.arange(-2, 3, 1)
    print(wave)
    y = np.arange(-2, 3, 1)
    X, Y = np.meshgrid(x, y)
    fig = plt.figure(figsize=(6, 6))
    Z = np.array(wave)
    ax = fig.add_subplot(111, projection='3d')

    ax.plot_surface(X, Y, Z)
    plt.show()


def first_plane_test():
    v_cell = VCell(0.7511)
    layer = first_layer.FirstLayer(v_cell, 0.5, 0.7511, radius=2, active_neighbs=3, theta=0.5)
    for i in range(0, 10):
        layer.build(train_images()[i])
        print(train_labels()[i])
        print("{}) на данный момент имеется {} признаков".format(i, len(layer.planes)))
        print("из них активировалось {} больше чем на одну клетку".format(
            len([plane for plane in layer.planes if np.count_nonzero(plane.to_ints()) > 1])))
        # plt.show()
        # input()
    file_work.save_to_file(layer)
    pass


# test_mh_scalar()
# first_plane_test()
# print(*[0, 0, 0])
a = file_work.load_from_file("model_1588682405.ncgm")
file_work.save_to_file(a)
print("a")
